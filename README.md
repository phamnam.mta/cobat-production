# CoBAT

CoBAT(Conversation-Based AI Assistant Teaching Tool) a tool for designing and teaching AI assistants

## Requirements

CoBAT production version only requires a recent version of Docker. You can install the Docker with the following:

### Installing docker

1. Set up the repository

```bash
# Install packages to allow apt to use a repository over HTTPS:
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

# Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add the Docker repository to APT sources:
sudo add-apt-repository  "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs) stable"

# Next, update the package database with the Docker packages from the newly added repo:
sudo apt-get update
```

2. Install Docker Community Edition(CE)

```bash
# Update the apt package index.
sudo apt-get update

# Install the latest version of Docker CE
sudo apt-get -y install docker-ce

# Docker should now be installed, the daemon started, and the process enabled to start on boot. Check that it's running:
sudo systemctl status docker
```

2. Installing docker-compose

```bash
# Run this command to download the Docker Compose version 1.24.1:
sudo curl -L https://github.com/docker/compose/releases/download/1.24.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

# Apply executable permissions to the binary:
sudo chmod +x /usr/local/bin/docker-compose

# Test the installation:
docker-compose --version
```

### Install and use CoBAT

1. Clone this repository:

```bash
git clone https://gitlab.com/phamnam.mta/cobat-production.git
cd cobat-production
```

2. Run CoBAT:

```bash
sudo bash ./run.sh
```

**Note:** CoBAT will be available at http://localhost:3000 so open your browser and happy editing.

3. Stop CoBAT

```bash
sudo bash ./stop.sh
```