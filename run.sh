#!/bin/bash

if [ $(stat -c '%u' agents) != 1001 ]; then
  sudo chown -R 1001:0 agents
fi

sudo docker-compose up -d --build
