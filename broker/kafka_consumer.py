import json
import logging
import os
from sgqlc.endpoint.http import HTTPEndpoint
import urllib.error

from kafka import KafkaConsumer, TopicPartition, OffsetAndMetadata
from constants import INSERT_UTTERANCE

logger = logging.getLogger(__name__)

class Receiver:
    def __init__(self):
        self.url = os.environ.get("BF_URL", "http://localhost:3000/graphql")
        self.graphql_endpoint = HTTPEndpoint(self.url)
        self.environment = os.environ.get("COBAT_ENV", "development")
        with open("config.json", "r") as conf_file:
            config = json.load(conf_file)
            self.kafka_bootstrap_servers = config["KAFKA_BOOTSTRAP_SERVERS"] 
            self.kafka_topic = config["KAFKA_TOPIC"]
            self.group_id = config["CONSUMER_GROUP_ID"]
            self.project_id = config["project_id"]
            self.username = config["KAFKA_SASL_USERNAME"]
            self.password = config["KAFKA_SASL_PASSWORD"]
            conf_file.close()

            logger.debug(f'Read config: {json.dumps(config, ensure_ascii=False, indent=2)}')

    def init_consumer(self):
        conf = {
            'bootstrap_servers': self.kafka_bootstrap_servers,
            'group_id': self.group_id,
            'enable_auto_commit': False,
            'security_protocol': 'SASL_PLAINTEXT',
            'sasl_mechanism': 'PLAIN',
            'sasl_plain_username': self.username,
            'sasl_plain_password': self.password
        }

        consumer = KafkaConsumer(
            **conf
        )

        consumer.assign(
            [
                TopicPartition(self.kafka_topic, 0),
                TopicPartition(self.kafka_topic, 1),
                TopicPartition(self.kafka_topic, 2),
                TopicPartition(self.kafka_topic, 3)
            ]
        )
        consumer.seek_to_beginning()
        
        return consumer

    def _graphql_query(self, query, params):
        try:
            response = self.graphql_endpoint(query, params)
            if response.get("errors"):
                raise urllib.error.URLError(
                    ", ".join([e.get("message")
                               for e in response.get("errors")])
                )
            return response.get("data")
        except urllib.error.URLError as e:
            message = e.reason
            logger.error(
                f"Something went wrong from {self.url}: {message}"
            )
            return {}

    def _insert_utterance_gql(self, sender_id, event):
        data = self._graphql_query(
            INSERT_UTTERANCE,
            {
                "senderId": sender_id,
                "projectId": self.project_id,
                "event": event,
                "env": self.environment,
            },
        )
        return data.get("insertUtterance")

    def _process_message(self, *args) -> bool:
        """
        process message here:
            - do some processing operators
            - insert msg to cobat database
            - commit to kafka
        :param args: list of rasa event consumed from kafka
        :return: true if msg is processed successfully, false if failed
        """
        for msg in args:
            try:
                event = json.loads(msg.value)
            except ValueError:
                continue
            event_type = event.get('event', '')
            sender_id = event.get('sender_id', '')
            channel = event.get('input_channel', '')
            if event_type == 'user' and channel == 'messenger':
                for e in event['parse_data']['entities']:
                    if e['entity'] == 'tmp_link':
                        event['text'] = event['text'].replace(e['value'], '').strip()
                        event['parse_data']['text'] = event['parse_data']['text'].replace(e['value'], '').strip()
                
                event['parse_data']['entities'] = [e for e in event['parse_data']['entities'] if e['entity'] != 'tmp_link']
                response = self._insert_utterance_gql(sender_id, event)
                if response['status'] != '201':
                   return False
                logger.debug(msg.partition, msg.offset, event["text"])

        return True

    def run(self, consumer: KafkaConsumer):
        try:
            while True:
                offset_by_partition = {}
                batch_msg = []
                for _ in range(3):
                    msg = next(consumer)
                    offset_by_partition[msg.partition] = max(msg.offset, offset_by_partition.get(msg.partition, 0))
                    batch_msg.append(msg)

                res = self._process_message(*batch_msg)
                if res is True:
                    # commit the last offset
                    for part, os in offset_by_partition.items():
                        consumer.commit(
                            {
                                TopicPartition(self.kafka_topic, part): OffsetAndMetadata(os + 1, None)
                            }
                        )
                    continue
                break
        except Exception as e:
            logger.error(e)
        finally:
            consumer.close(autocommit=False)


if __name__ == '__main__':
    receive = Receiver()
    consumer = receive.init_consumer()
    receive.run(consumer)
