import os
import asyncio
import logging
import json
import time
import urllib.error
from sgqlc.endpoint.http import HTTPEndpoint
from azure.eventhub.aio import EventHubConsumerClient
from azure.eventhub.extensions.checkpointstoreblobaio import BlobCheckpointStore
from constants import INSERT_UTTERANCE, INSERT_TRACKER

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

CONNECTION_STR = os.environ.get("CONNECTION_STR")
EVENT_HUB_NAME = os.environ.get("EVENT_HUB_NAME")
CONSUMER_GROUP = os.environ.get("CONSUMER_GROUP")
STORAGE_CONNECTION_STR = os.environ.get("STORAGE_CONNECTION_STR")
BLOB_CONTAINER_NAME = os.environ.get("BLOB_CONTAINER_NAME")

class Receiver:
    def __init__(self):
        self.url = os.environ.get("BF_URL", "http://localhost:3000/graphql")
        self.graphql_endpoint = HTTPEndpoint(self.url)
        self.environment = os.environ.get("COBAT_ENV", "development")
        self.project_id = os.environ.get("BF_PROJECT_ID")
        self.client = None

    def init_consumer(self):
        checkpoint_store = BlobCheckpointStore.from_connection_string(STORAGE_CONNECTION_STR, BLOB_CONTAINER_NAME)
        self.client = EventHubConsumerClient.from_connection_string(
            conn_str=CONNECTION_STR,
            consumer_group=CONSUMER_GROUP,
            eventhub_name=EVENT_HUB_NAME,
            checkpoint_store=checkpoint_store
        )
        logger.debug("Consumer has been initialized")

    def _graphql_query(self, query, params):
        try:
            response = self.graphql_endpoint(query, params)
            if response.get("errors"):
                raise urllib.error.URLError(
                    ", ".join([e.get("message")
                               for e in response.get("errors")])
                )
            return response.get("data")
        except urllib.error.URLError as e:
            message = e.reason
            logger.error(
                f"Something went wrong from {self.url}: {message}"
            )
            return {}

    def _insert_utterance_gql(self, sender_id, event):
        data = self._graphql_query(
            INSERT_UTTERANCE,
            {
                "senderId": sender_id,
                "projectId": self.project_id,
                "event": event,
                "env": self.environment,
            },
        )
        return data.get("insertUtterance")

    async def on_error(self, partition_context, error):
        if partition_context:
            logger.error("An exception: {} occurred during receiving from Partition: {}.".format(
                partition_context.partition_id,
                error
            ))
        else:
            logger.error("An exception: {} occurred during the load balance process.".format(error))
        await self.client.close()

    async def on_event(self, partition_context, event):
        """
        process message here:
            - do some processing operators
            - insert msg to cobat database
            - commit to checkpoint
        :param args: list of rasa event consumed from kafka
        :return: true if msg is processed successfully, false if failed
        """
        data = event.body_as_json(encoding='UTF-8')
        logger.debug("Received the event: \"{}\" from the partition with ID: \"{}\"".format(data, partition_context.partition_id))

        event_type = data.get('event')
        sender_id = data.get('sender_id')
        text = data.get('text')
            
        if event_type == 'user' and sender_id and text and not text.startswith("/"):
            response = self._insert_utterance_gql(sender_id, data)
            if response['status'] == '201':
                await partition_context.update_checkpoint(event)
                time.sleep(0.01)
        

    async def run(self):
        async with self.client:
            logger.debug("Start listening events from Eventhub")
            await self.client.receive(
                on_event=self.on_event,
                on_error=self.on_error,
                starting_position="-1",
            )


if __name__ == '__main__':
    receive = Receiver()
    receive.init_consumer()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(receive.run())