import json
import time

with open("fake_events.json", "r") as f:
    events = json.load(f)
    f.close()

from kafka import KafkaProducer

KAFKA_BOOTSTRAP_SERVERS = 'localhost:9092'
KAFKA_TOPIC = 'ks_rasa_events'

producer = KafkaProducer(
    bootstrap_servers=KAFKA_BOOTSTRAP_SERVERS,
    value_serializer=lambda v: json.dumps(v).encode('utf-8')
)

while True:
    for evt in events:
        future = producer.send(KAFKA_TOPIC, evt)
        res = future.get(30)
        print(res)
    time.sleep(10)
