#!/bin/bash

iptables -tnat -A OUTPUT -d 192.168.20.15 -j DNAT --to-destination 103.74.123.175 -m comment --comment "Kafka production"
python -u kafka_consumer.py