INSERT_UTTERANCE = """
mutation insertUtterance(
    $senderId: String!
    $projectId: String!
    $event: Any
    $env: Environment
) {
    insertUtterance(senderId: $senderId, projectId:$projectId, event:$event, env: $env){
        status
    }
}
"""

INSERT_TRACKER = """
mutation insertTracker(
    $senderId: String!
    $projectId: String!
    $tracker: Any
    $env: Environment
) {
    insertTrackerStore(senderId: $senderId, projectId:$projectId, tracker:$tracker, env: $env){
        lastIndex
        lastTimestamp
    }
}
"""