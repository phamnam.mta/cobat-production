#!/usr/bin/env python3

import os
import logging
import json
import pika
from sgqlc.endpoint.http import HTTPEndpoint
import urllib.error

logging.basicConfig(format="%(asctime)s %(message)s", level=logging.INFO)

INSERT_UTTERANCE = """
mutation insertUtterance(
    $senderId: String!
    $projectId: String!
    $event: Any
    $env: Environment
) {
    insertUtterance(senderId: $senderId, projectId:$projectId, event:$event, env: $env){
        status
    }
}
"""


class Receiver:
    def __init__(self):

        self.project_id = "bf"  # os.environ.get("BF_PROJECT_ID")
        self.url = "http://localhost:3000/graphql"  # os.environ.get("BF_URL")
        self.graphql_endpoint = HTTPEndpoint(self.url)
        self.environment = os.environ.get("COBAT_ENV", "development")

    def _graphql_query(self, query, params):
        try:
            response = self.graphql_endpoint(query, params)
            if response.get("errors"):
                raise urllib.error.URLError(
                    ", ".join([e.get("message")
                               for e in response.get("errors")])
                )
            return response.get("data")
        except urllib.error.URLError as e:
            message = e.reason
            logging.error(
                f"Something went wrong from {self.url}: {message}"
            )
            return {}

    def _insert_utterance_gql(self, sender_id, event):
        data = self._graphql_query(
            INSERT_UTTERANCE,
            {
                "senderId": sender_id,
                "projectId": self.project_id,
                "event": event,
                "env": self.environment,
            },
        )
        return data.get("insertUtterance")

    def generateCallback(self):
        def callback(ch, method, properties, body):
            event = json.loads(body)
            sender_id = event.get('sender_id', '')
            channel = event.get('input_channel', '')
            if event['event'] == 'user' and channel == 'facebook' and sender_id:
                try:
                    response = self._insert_utterance_gql(sender_id, event)
                    logging.info(response)
                except Exception as e:
                    logging.error(e)

        return callback


def event_consumer(callback, host, username, password, queue, exchange):
    logging.info(host)
    credentials = pika.PlainCredentials(username, password)
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host,
                                  credentials=credentials))

    channel = connection.channel()

    channel.queue_declare(queue, durable=True)
    channel.queue_bind(exchange=exchange, queue=queue)

    channel.basic_consume(queue, callback, auto_ack=True)
    channel.start_consuming()


if __name__ == "__main__":
    receive = Receiver()
    callback = receive. generateCallback()
    host = "192.168.20.61"  # os.environ.get("RABBITMQ_HOST")
    username = "salebot"  # os.environ.get("RABBITMQ_USERNAME")
    password = "123456"  # os.environ.get("RABBITMQ_PASSWORD")
    queue = "rasa_event"  # os.environ.get("RABBITMQ_QUEUE")
    exchange = "rasa_event"  # os.environ.get("RABBITMQ_EXCHANGE")

    event_consumer(callback, host, username, password, queue, exchange)
